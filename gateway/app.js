const express = require("express");
const cors = require("cors");
const proxy = require("express-http-proxy");
require("dotenv").config();

const app = express();

app.use(express.json());
app.use(cors());

app.use("/customer", proxy("http://localhost:8001"));
app.use("/shopping", proxy("http://localhost:8003"));
app.use("/product", proxy("http://localhost:8002"));

const PORT = 8000;

app.listen(PORT, () => {
  console.log(`server is Listening on ${PORT}`);
});
