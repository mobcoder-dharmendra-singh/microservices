const mongoose = require("mongoose");

const WishListSchema = new mongoose.Schema({
  userId: { type: mongoose.Types.ObjectId, required: true },
  productName: { type: String, required: true },
  productPrice: { type: Number, required: true },
  productQty: { type: Number, required: true },
});

const model = mongoose.model("wishList", WishListSchema);

module.exports = model;
