var jwt = require("jsonwebtoken");

module.exports = (req, res, next) => {
  try {
    const token = req.headers.authorization.split(" ")[1];
    // console.log("---Token-->", token);
    const verifyToken = jwt.verify(token, process.env.JWTSECRETKEY);
    // console.log("---verify token--->>", verifyToken);
    next();
  } catch (err) {
    return res.status(401).json({
      msg: "Invalid Token",
    });
  }
};
