const express = require("express");
const app = express();

const router = express.Router();

const {
  addToWishList,
  displayAllWishListItem,
} = require("../controllers/wishListController");

router.post("/addToWishList", addToWishList);

router.get("/displayWishListItem", displayAllWishListItem);

module.exports = router;
