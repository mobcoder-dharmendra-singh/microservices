const WishList = require("../models/wishList");

const addToWishList = (req, res) => {
  const { userId, productName, productPrice, productQty } = req.body;

  const wishList = new WishList({
    userId,
    productName,
    productPrice,
    productQty,
  });

  wishList
    .save()
    .then((result) => {
      return res.status(200).json({
        status: 200,
        data: result,
        message: "Add to wishlist",
      });
    })
    .catch((err) => {
      return res.status(500).json({
        staus: 500,
        message: "server error",
        error: err,
      });
    });
};

const displayAllWishListItem = async (req, res) => {
  try {
    // let userId = req.params.id;
    let data = await WishList.find({});
    if (data) {
      return res.status(200).json({
        status: 200,
        message: "Record Found",
        data: data,
      });
    } else {
      return res.status(200).json({
        status: 404,
        message: "Record Not Found",
        data: data,
      });
    }
  } catch (err) {
    return res.status(500).json({
      status: 500,
      error: err,
    });
  }
};

module.exports = {
  addToWishList,
  displayAllWishListItem,
};
