const express = require("express");
const app = express();

const router = express.Router();

const { registerUser, login } = require("../controllers/userController");

const checkAuth = require("../middleware/check-auth");

//Register User
router.post("/registerUser", registerUser);

//login user
router.post("/login", login);

module.exports = router;
