const User = require("../models/user");
var jwt = require("jsonwebtoken");

const registerUser = (req, res) => {
  const { userName, password, firstName, lastName, phoneNumber } = req.body;

  const user = new User({
    userName,
    password,
    firstName,
    lastName,
    phoneNumber,
    created_at: Date.now(),
    modified_at: Date.now(),
  });

  user
    .save()
    .then((result) => {
      return res.status(200).json({
        status: 200,
        data: result,
        message: "Record Submitted",
      });
    })
    .catch((err) => {
      return res.status(500).json({
        staus: 500,
        message: "server error",
        error: err,
      });
    });
};

const login = async (req, res) => {
  try {
    let { userName, password } = req.body;
    let data = await User.findOne({ userName });

    if (data) {
      if (data?.password == password) {
        return res.status(200).json({
          message: "user found",
          staus: 200,
          data: data,
        });
      } else {
        return res.status(400).json({
          message: "password not match",
          staus: 400,
        });
      }
    } else {
      return res.status(404).json({
        message: "User not found",
        staus: 404,
      });
    }

    // if (data) {
    //   if (data?.password == password) {
    //     return res.status(200).json({
    //       message: "user found",
    //       staus: 200,
    //       token: token,
    //     });
    //   } else {
    //     return res.status(400).json({
    //       message: "password not match",
    //       staus: 400,
    //     });
    //   }
    // } else {
    //   return res.status(404).json({
    //     message: "User not found",
    //     staus: 404,
    //   });
    // }
  } catch (err) {
    return res.status(500).json({
      message: "Server Error",
      staus: 500,
    });
  }
};

module.exports = {
  registerUser,
  login,
};
