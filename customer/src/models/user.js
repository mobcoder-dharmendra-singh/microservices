const mongoose = require("mongoose");

const UserSchema = new mongoose.Schema(
  {
    userName: { type: String, required: true, unique: true },
    password: { type: String, required: true },
    firstName: { type: String, required: true },
    lastName: { type: String, required: true },
    phoneNumber: { type: String, required: true },
    created_at: { type: Number, required: true },
    modified_at: { type: Number, required: true },
  },
  {
    collection: "user",
  }
);

const model = mongoose.model("user", UserSchema);

module.exports = model;
