const express = require("express");
const cors = require("cors");
const mongoose = require("mongoose");
require("dotenv").config();

const app = express();

app.use(express.json());
app.use(cors());
app.set("view engine", "ejs");

const PORT = 8002;

mongoose.connect(
  "mongodb://localhost:27017/microservice_demo",
  { useNewUrlParser: true, useUnifiedTopology: true },
  function (err) {
    if (err) {
      console.log("DB Error: ", err);
      process.exit(1);
    } else {
      console.log("MongoDB Connected");
    }
  }
);

app.use("/", require("./src/routes/productRoutes"));

app.listen(PORT, () => {
  console.log(`server is Listening on ${PORT}`);
});
