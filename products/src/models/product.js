const mongoose = require("mongoose");

const ProductSchema = new mongoose.Schema({
  productName: { type: String },
  productMRP: { type: Number },
  productOfferPrice: { type: Number },
  Qty: { type: Number },
  created_at: { type: String },
});

const model = mongoose.model("Product", ProductSchema);

module.exports = model;
