const Product = require("../models/product");

const registerProduct = (req, res) => {
  const { productName, productMRP, productOfferPrice, Qty } = req.body;

  const product = new Product({
    productName,
    productMRP,
    productOfferPrice,
    Qty,
    created_at: Date.now(),
  });

  product
    .save()
    .then((result) => {
      return res.status(200).json({
        status: 200,
        data: result,
        message: "Record Submitted",
      });
    })
    .catch((err) => {
      return res.status(500).json({
        staus: 500,
        message: "server error",
        error: err,
      });
    });
};

const displayAllProduct = async (req, res) => {
  try {
    let data = await Product.find({});
    if (data.length == 0) {
      return res.status(200).json({
        status: 404,
        message: "Record Not Found",
        data: data,
      });
    } else if (data.length > 1) {
      return res.status(200).json({
        status: 200,
        message: "Record Found",
        data: data,
      });
    }
  } catch (err) {
    return res.status(500).json({
      status: 500,
      error: err,
    });
  }
};

module.exports = {
  registerProduct,
  displayAllProduct,
};
