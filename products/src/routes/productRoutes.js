const express = require("express");
const app = express();

const router = express.Router();

const {
  registerProduct,
  displayAllProduct,
} = require("../controllers/productController");

router.post("/registerProduct", registerProduct);

router.get("/displayAllProduct", displayAllProduct);

module.exports = router;
